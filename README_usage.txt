Copyright (c) 2013, Lawrence Livermore National Security, LLC. 
Produced at the Lawrence Livermore National Laboratory. 
Written by Henry Hsieh <hsieh7@llnl.gov> and Brian Van Essen <vanessen1@llnl.gov>. 
LLNL-CODE-624712. 
All rights reserved.

This file is part of LRIOT, Version 1.0. 
For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html

--------------------------------------------------------------------------------

LRIOT Options and Defaults

Commandline only options:
  --version             Print version
  -h [ --help ]         Print help message
  -c [ --config ] arg   Name of the config file
  -v [ --validate ]     Validate I/O
  -u [ --unique ]       Enforce unique sequences for a single processes

Benchmark options:
  -n [ --processes ] arg Number of processes
  -t [ --threads ] arg   Number of threads per process
  -i [ --NIOP ] arg      Total number of I/O operations
  -m [ --mode ] arg      I/O mode select from [mmap,direct,std]
  -s [ --size ] arg      Size of memory access in bytes only used in std io
  -k [ --read ] arg      Percent of accesses that are reads for each file
                         [0-1], delineated by :
  -d [ --delay ] arg     Percent of I/O operations before timing starts [0-1]
  -f [ --filename ] arg  Files to be accessed, delineated by :
  -p [ --pattern ] arg   Pattern to generate [uni,norm, or
                         'seq|stride|end|jump|thread_offset'],delineated by :
                         e.g. 'seq|1|*|1|1'
  -o [ --offset ] arg    Offset into each file, delineated by :
  -r [ --range ] arg     Ranges for each pattern, delineated by :
  -w [ --weight ] arg    Weights of of each pattern, delineated by :
  -e [ --seed ] arg      Seed for random generation


default settings
--processes 1
--threads 4
--NIOP 1000
--mode mmap
--size 4096
--read *1                                        // 1 for all files
--delay 0.1
--filename <NONE>
--pattern *uni                                // uniform random distribution 
--offset *0                                     // no offset for all files
--range *                                       // set to file size
--weight *1                                   // weight of 1 for all files
--seed 123

 

Profile examples:
--------------------------------------------------------------------------------

--------------------------------------------------------------------------------
Single Profile:  
--------------------------------------------------------------------------------

50k I/O operations - 100% reads over 8GB of a 8GB(or larger) file using 4 threads

        ./LRIOT -i 50000 -f /file1

        ------------------------------------OR------------------------------------

        #config file

        name = profile0
        processes = 1
        threads = 4
        NIOP = 50000
        mode = mmap
        size = 8
        read = 1
        delay = 0.06
        filename = /file1
        pattern = uni
        offset = *
        range =  *
        weight =  *

        #end config file

        ./LRIOT -c lriot.cfg profile0

100k I/O operations - 80% read, 20% write over 8GB of a 8GB(or larger) file using 8 threads

        ./LRIOT -i 100000 -f /file1 -k 0.8 -t 8

        ------------------------------------OR------------------------------------

        #config file

        name = profile1
        processes = 2
        threads = 8
        NIOP = 100000
        mode = mmap
        size = 8
        read = 0.8
        delay =  *
        filename = /file1
        pattern =  *
        offset =  *
        range =  *
        weight =  *

        #end config file

        ./LRIOT -c lriot.cfg profile1

 

100k read I/O operations over 2 partitions of a single file
     partition1 - 10% of accesses in the first 7 GB of the file
     partition2 - 90% of accesses in the last 1 GB of the file 

        ./LRIOT -i 100000 -f /file1:/file1 -o 0:7GB -r 7GB:1GB -w 10:90

        ------------------------------------OR------------------------------------

        #config file

        name = profile2
        processes =  *
        threads = 8
        NIOP = 100000
        mode = mmap
        size =  *
        read =  *
        delay =  *
        filename = /file1:/file1
        pattern =  *
        offset = 0:7GB
        range = 7GB:1GB
        weight = 10:90  //1:9, 0.1:0.9 are also allowed

        #end config file

        ./LRIOT -c lriot.cfg profile2

100k I/O operations over 2 files - 2 processes, 4 threads each
     file1 - 10% of accesses, entire file, 90% reads 
     file2 - 90% of accesses, last 1GB of a 8 GB file, 50% reads

        ./LRIOT -i 100000 -f /file1:file2 -w 10:90 -k 0.9:0.5 -n 2 -o -7GB -r -:1GB 

        ------------------------------------OR------------------------------------

        #config file

        name = profile3
        processes = 2
        threads = 4
        NIOP = 100000
        mode = mmap
        size =  *
        read = 0.9:0.5
        delay =  *
        filename = /file1:/file2
        pattern =  *
        offset = -:7GB
        range = -:1GB
        weight = 10:90  //1:9, 0.1:0.9 are also allowed

        #end config file

        ./LRIOT -c lriot.cfg profile3

--------------------------------------------------------------------------------
Multiple Profiles:
--------------------------------------------------------------------------------

2 Profiles 
        profile0 - 50k I/O operations - 100% reads over 8GB of a 8GB(or larger) file using 4 threads
        profile3 - 100k I/O operations over 2 files - 2 processes, 4 threads each
                                 file1 - 10% of accesses, entire file, 90% reads
                                 file2 - 90% of accesses, last 1GB of a 8 GB file, 50% reads
        #--------------config file------------------

        name = profile0
        processes =  *
        threads = 4
        NIOP = 50000
        mode = mmap
        size =  *
        read =  *
        delay =  *
        filename = /file1
        pattern =  *
        offset =  *
        range =  *
        weight =  *

        #------------------------------------

        name = profile3
        processes = 2
        threads = 4
        NIOP = 100000
        mode = mmap
        size =  *
        read = 0.9:0.5
        delay =  *
        filename = /file1:/file2
        pattern =  *
        offset = -:7GB
        range = -:1GB
        weight = 10:90  //1:9, 0.1:0.9 are also allowed

        #-----------end config file--------------

        ./LRIOT -c lriot.cfg profile0:profile3

--------------------------------------------------------------------------------

