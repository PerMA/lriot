/*
 * Copyright (c) 2013, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Henry Hsieh <hsieh7@llnl.gov> and Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-624712. 
 * All rights reserved.
 * 
 * This file is part of LRIOT, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Additional BSD Notice. 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * • Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the disclaimer below.
 * 
 * • Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the disclaimer (as noted below) in the
 *   documentation and/or other materials provided with the distribution.
 * 
 * • Neither the name of the LLNS/LLNL nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL LAWRENCE LIVERMORE NATIONAL SECURITY, LLC,
 * THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 * Additional BSD Notice
 * 
 * 1. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at Lawrence Livermore
 * National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * 2. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or
 * process disclosed, or represents that its use would not infringe
 * privately-owned rights.
 * 
 * 3. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring
 * by the United States Government or Lawrence Livermore National Security,
 * LLC. The views and opinions of authors expressed herein do not necessarily
 * state or reflect those of the United States Government or Lawrence Livermore
 * National Security, LLC, and shall not be used for advertising or product
 * endorsement purposes.
 * 
 */
#ifndef WEIGHTED_RANDOM
#define WEIGHTED_RANDOM

#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
#include<boost/random.hpp>

#include <iostream>
#include <string>
#include <vector>
#include <iterator>

typedef enum{READ, WRITE} access_type;

//memory_access.first is the access type
//memory_access.second is the offset
typedef std::pair<access_type, uint64_t> memory_access;

//file_access.first will represent the fileid, or fd
//file_access.second wil be the access for that file
typedef std::pair<int, memory_access> file_access;


//interface for other types of generators
class generator_wrapper
{
    public:
        //sould probably rename this... see seq_wrapper
        virtual memory_access gen_maccess() = 0;
        virtual std::string type() = 0;
        virtual void summary() =0 ; 
};
//not really a wrapper...;
class seq_wrapper : public generator_wrapper
{
    public: 
        seq_wrapper( uint64_t _start, uint64_t _stride, uint64_t _end, 
            uint64_t _jump, uint64_t _thread_offset, int _seed, 
            double _read, int _tid );
        memory_access gen_maccess();  
        void summary();
        std::string type();
        std::vector<double> RW_weights();  //get the READ/WRITE weights

    private:
        int tid; 
        uint64_t next_end; 
        uint64_t next_start;
        uint64_t current; 

        //see wiki page for info on defination
        uint64_t start;
        uint64_t stride;
        uint64_t end;
        uint64_t jump;
        uint64_t thread_offset;
        
        //used to detrmine r/w
        boost::random::discrete_distribution<>  access_generator;
        boost::mt19937 generator;
};


//TODO: normal distribution

//uniform generation
//----------------------
//1 generator engine, 2 distribution(offset/address, and read or write)
class uniform_wrapper : public generator_wrapper
{
    public:
        uniform_wrapper(int seed, double read, uint64_t _offset, uint64_t range);
        memory_access gen_maccess();
        uint64_t min();  //return min
        uint64_t max();  //return max
        std::vector<double> RW_weights();
        std::string type();
        void summary();

    private:
        boost::mt19937 generator;
        boost::variate_generator<boost::mt19937,
            boost::uniform_int<uint64_t> > offset_generator;
        boost::random::discrete_distribution<>  access_generator;
        uint64_t offset;
};

//holds generator wrappers for indvidual generators 
//uses uses weighted random generator to select the next generator to use
class weighted_random
{
    public:
        weighted_random(int seed, int _nfiles, 
            std::vector<std::string> pattern_list,
            std::vector<double> read_list, 
            std::vector<uint64_t> offset_list, 
            std::vector<uint64_t> range_list, 
            std::vector<double> _weight_list,
            uint64_t scale, int tid);

        ~weighted_random();
        void summary();
        file_access gen_faccess();

    private:
        boost::mt19937 generator;
        std::vector<double> weight_list;
        std::vector<generator_wrapper*> generator_list;
        boost::random::discrete_distribution<>  generator_selector;
        int nfiles;
};
#endif
