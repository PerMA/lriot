/*
 * Copyright (c) 2013, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Henry Hsieh <hsieh7@llnl.gov> and Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-624712. 
 * All rights reserved.
 * 
 * This file is part of LRIOT, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Additional BSD Notice. 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * • Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the disclaimer below.
 * 
 * • Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the disclaimer (as noted below) in the
 *   documentation and/or other materials provided with the distribution.
 * 
 * • Neither the name of the LLNS/LLNL nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL LAWRENCE LIVERMORE NATIONAL SECURITY, LLC,
 * THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 * Additional BSD Notice
 * 
 * 1. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at Lawrence Livermore
 * National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * 2. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or
 * process disclosed, or represents that its use would not infringe
 * privately-owned rights.
 * 
 * 3. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring
 * by the United States Government or Lawrence Livermore National Security,
 * LLC. The views and opinions of authors expressed herein do not necessarily
 * state or reflect those of the United States Government or Lawrence Livermore
 * National Security, LLC, and shall not be used for advertising or product
 * endorsement purposes.
 * 
 */
#include "profile.hpp"
#include "mpi.h"
#include <fstream>
#include <sys/time.h>
#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <errno.h>
#include <omp.h>
#include <boost/unordered_set.hpp>

#define THREAD_PRIME 1601
#define PAGE_SIZE 4096
#define MMAP_SIZE 8
#define RETRIES 100000000

extern int MPI_rank, MPI_size;

using namespace std;

profile::profile(int num_processes, int process_offset, string mode, int num_threads, 
    int seed, string names,string patterns, string reads, string offsets, string ranges,
    string weights, uint64_t _niops, int MPI_local_rank, string _name, bool val_flag, bool _unique,
    double _delay,uint64_t _access_size)
:niops(_niops), name(_name), nthreads(num_threads), nprocesses(num_processes),
    validate(val_flag), p_offset(process_offset),delay(_delay), skipped_ioops(_delay*_niops),
    access_size(_access_size), bad(false), unique(_unique)
{
    //vectors to convert for generator
    //vector<string> name_list, pattern_list; private variables
    vector<string> weight_list_str, offset_list_str, read_list_str, range_list_str;
    vector<uint64_t> offset_list;
    vector<double> weight_list, read_list;

    //tokenize 
    boost::split(name_list,names, boost::is_any_of(":"), boost::token_compress_on );
    boost::split(pattern_list,patterns, boost::is_any_of(":"), boost::token_compress_on );

    boost::split(weight_list_str,weights, boost::is_any_of(":"), boost::token_compress_on );
    boost::split(offset_list_str,offsets, boost::is_any_of(":"), boost::token_compress_on );
    boost::split(range_list_str,ranges, boost::is_any_of(":"), boost::token_compress_on );
    boost::split(read_list_str,reads, boost::is_any_of(":"), boost::token_compress_on );

    //set flags based on modes
    if(mode == "mmap")
    {
    //    cout<<"mmap mode"<<endl; 
        mmap_mode = true;
        direct_mode = false;
	rmw_mode = false;
    }
    else if(mode == "direct")
    {
      //  cout<<"direct mode"<<endl; 
        mmap_mode = false;
        direct_mode = true; 
	rmw_mode = false;
    }
    else if(mode == "direct_rmw")
    {
      //  cout<<"direct mode"<<endl; 
        mmap_mode = false;
        direct_mode = true;
	rmw_mode = true;
    }
    else if(mode == "std")
    {
        //cout<<"std mode"<<endl; 
        mmap_mode = false;
        direct_mode = false; 
	rmw_mode = false;
    }
    
    //use filenames to determine the number of files
    int nfiles = name_list.size();

    int tmp_fd;
    struct stat fstat;
    mode_t options; 

    for (int i = 0; i<nfiles; i++)
    {
        //open
        if (direct_mode)
            options = O_RDWR | O_LARGEFILE | O_DIRECT;
        else
            options = O_RDWR | O_LARGEFILE;

        tmp_fd = open(name_list[i].c_str(), options);
        if (tmp_fd >0)
            fd_vec.push_back(tmp_fd);
        else
        {
            cout<<MPI_rank<<": ERROR, open failed on file "<<name_list[i]<<" with error, " <<strerror(errno)  <<endl<<flush;
            bad = true;
            return;
        }
    
        //stat for filesize
        stat(name_list[i].c_str(), &fstat);
	char name[128];
	int namelen = 0;
	MPI_Get_processor_name(name,&namelen);
       
        cout<<"MPI Rank " << MPI_rank << " on node " << name << " opened " << name_list[i]<<", file size: "<<fstat.st_size<<endl;
       
        //if 1st is * or current is -, use file size, else use input, or spec is larger than file size
        if(range_list_str[0][0]=='*' || range_list_str[i][0] == '-' )
        {
            if(fstat.st_size == 0)
            {
                if(MPI_rank == p_offset)
                    cout<<"ERROR: stat cannot detect size"<<endl;
                bad = true;
                return;
            }
            else
                mmap_size_vec.push_back(fstat.st_size);
        }
        else
        {
            uint64_t rng = boost::lexical_cast<uint64_t>(range_list_str[i]);
            if (rng > fstat.st_size && fstat.st_size!=0) 
                mmap_size_vec.push_back(fstat.st_size);
            else
                mmap_size_vec.push_back(rng);
        }
        
        //if 1st is * or current is -, use 0, else use input
        if(offset_list_str[0][0]=='*' || offset_list_str[i][0] == '-')
            offset_list.push_back(0);
        else
            offset_list.push_back(boost::lexical_cast<uint64_t>(offset_list_str[i]));

        //if 1st is * or current is -, use 1, else use input
        if(weight_list_str[0][0]=='*' || weight_list_str[i][0] == '-')
            weight_list.push_back(1);
        else
            weight_list.push_back(boost::lexical_cast<double>(weight_list_str[i]));

        //if 1st is * or current is -, use 1, else use input
        if(read_list_str[0][0]=='*' || read_list_str[i][0] == '-')
            read_list.push_back(1);
        else
            read_list.push_back(boost::lexical_cast<double>(read_list_str[i]));
    }
    
    uint64_t scale = access_size; 
    if ( mmap_mode && !validate)
        scale = MMAP_SIZE;

//should be set in lriot.cpp
/*    if(validate|| !mmap_mode )
    {
        scale = PAGE_SIZE;
        if(MPI_rank ==0 & nprocesses*nthreads>(PAGE_SIZE/(ACCESS_SIZE)))
        {
            cout<<"Validate mode: Number of threads is greater than the number of entries on a page, may result in race conditions"<<endl;
        }
    }
    if (!mmap_mode) 
    {
        scale = PAGE_SIZE;
        //cout<<"page aligned"<<endl;
    }
*/
    //create ngenerators 
    scale = PAGE_SIZE;
    int thread_count_offset = (MPI_rank-p_offset)*nthreads; 
    weighted_random* tmp; 
    for(int i=0 ; i<nthreads ; i++)
    {
        tmp = new weighted_random((seed+2)*(MPI_rank+seed) + (i*THREAD_PRIME), nfiles, pattern_list, read_list,
           offset_list, mmap_size_vec, weight_list, scale, thread_count_offset + i );
        generators.push_back(tmp);
    }
}

profile::~profile()
{
  /*  fstream tuningfd;
    tuningfd.open( "/proc/perma-ramdisk-tuning", fstream::in| fstream::out);
    if(tuningfd.good())
        tuningfd<<"mmap_flush_buffers"<<endl;
    else
        cout<<"ERROR flushing"<<endl;
    tuningfd.close();
*/
    cleanup_files();

    //delete generators
    for (int i = 0; i< generators.size() ; i++)
        delete generators[i];

}

//fill offsets one generator per thread
//slower than fill_offsets1
void profile::init()
{
    bool advise = false;
    MPI_Barrier(MPI_COMM_WORLD);
    uint64_t* tmp_addr;
    for (int i = 0; i<name_list.size(); i++)
    {
        //set up mmap
        if(mmap_mode )
        {
//            cout<<"trying size" <<mmap_size_vec[i]<<endl;
            tmp_addr = (uint64_t*) mmap(NULL, mmap_size_vec[i], PROT_READ | PROT_WRITE, MAP_SHARED, fd_vec[i], 0);
            //tmp_addr = (uint64_t*) mmap(NULL, mmap_size_vec[i], PROT_READ | PROT_WRITE, MAP_SHARED, fd_vec[i], 0);
            if( (void*) tmp_addr != MAP_FAILED)
            {
                mmap_addr_vec.push_back(tmp_addr);
                cout<<MPI_rank<<": has an mmap that starts at "<<tmp_addr <<" to "<< (tmp_addr+(mmap_size_vec[i]/8)-1)<<endl;
                //IF NEED TO ALLOCATE SPACE WITH NO PERMISSONS AFTER THIS
                //tmp_addr += mmap_size_vec[i]/8 - 4096;
                //tmp_addr = (uint64_t*) mmap(tmp_addr, mmap_size_vec[i], PROT_NONE, MAP_SHARED|MAP_FIXED, fd_vec[i], offset_list[i]);
                //cout<<MPI_rank<<": NNONE start at "<<tmp_addr <<" to "<< (tmp_addr+(mmap_size_vec[i]/8)-1)<<endl;
            }
            else
            {
                cout<<MPI_rank<<": error map failed for "<<name_list[i]<<" with error, "<<strerror(errno)<<endl;
                bad = true; 
                return;
            }
            if(advise)
            {
                if(MPI_rank == 0)
                    cout<<"USING MADVISE RANDOM"<<endl;
                //set up mmap_advise
                int err =madvise(tmp_addr, mmap_size_vec[i], MADV_RANDOM);
                if ( err<0)
                {
                    cout<<"MADVISE ERROR: "<<err<<endl;
                    //exit(1);
                }
            }
            else
            {
                if(MPI_rank == 0)
                    cout<<"NOT USING MADVISE RANDOM"<<endl;
            }
        }
    }


    MPI_Barrier(MPI_COMM_WORLD);

    timeval start, end;
    double elapsed_time;
    
    //set to threads to nthreads
    omp_set_num_threads(nthreads);

    gettimeofday(&start, NULL);
    //add vectors of memory_access for each thread
    for (int i= 0; i<nthreads; i++)
    {
        vector<file_access> tmp0;
        offsets.push_back(tmp0);
    }
    uint64_t count = 0;
    boost::unordered_set<uint64_t> offset = boost::unordered_set<uint64_t> ();
    //each thread will fill random offsets
    #pragma omp parallel shared(offset) private(count)
    {
        count = 0;
        file_access tmp;
        int tid = omp_get_thread_num();
        for(int i = 0; i<niops; i++)
        {
            tmp = generators[tid]->gen_faccess();
            if(unique)
            {
                #pragma omp critical
                {
                if(count < RETRIES)
                    count = 0;
                while (count<RETRIES && offset.find(tmp.second.second) != offset.end())
                {
                    //cout<<"REDO"<<endl;
                    tmp = generators[tid]->gen_faccess();
                    count++;
                }
                if (count ==  RETRIES)
                {
                    cout<<"Sequences will not be unique too many retries, failed with "<<RETRIES<<" retries"<<endl;
                    count++;
                }
                offset.insert(tmp.second.second);
                }
            }
            offsets[tid].push_back(tmp);
        }
    }
    gettimeofday(&end, NULL);

    elapsed_time =   double(end.tv_sec - start.tv_sec) + double(end.tv_usec - start.tv_usec)*1e-6;
    
    cout<<"Generation on "<<MPI_rank<<" took "<<elapsed_time<<" seconds"<<endl;

}

//validates writes, will ignore reads will be marked as writes
int profile::mmap_validate_io()
{
    omp_set_num_threads(nthreads);

    //time values
    timeval start, end;
    double elapsed_time, global_elapsed_time;
   
    int tid=0;
    int i;
    uint64_t failed = 0, global_failed = 0;
   
    MPI_Barrier(MPI_COMM_WORLD);
    gettimeofday(&start, NULL);

    int epp = PAGE_SIZE/MMAP_SIZE; // entries per page;
    int poff; //offset into the page

    #pragma omp parallel private(tid, i,poff) shared(failed,epp)
    for(i = 0; i<niops; i++)
    {   
        if (i == 0)
        {
            tid = omp_get_thread_num();
            poff = MPI_rank * nthreads + tid;
        }
        #pragma omp critical
        {
            if(mmap_addr_vec[offsets[tid][i].first][ ((offsets[tid][i].second.second)*epp) +poff ]!=0)
            failed++;
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);
    
    #pragma omp parallel private(tid, i,poff) shared(epp)
    for(i = 0; i<niops; i++)
    {
        if (i == 0)
        {
            tid = omp_get_thread_num();
            poff = MPI_rank * nthreads + tid;
        }
        mmap_addr_vec[offsets[tid][i].first][ ((offsets[tid][i].second.second)*epp) + poff ]++;
    }

    MPI_Barrier(MPI_COMM_WORLD);
    
    if (MPI_rank==0)
        cout<<"Done incrementing"<<endl;
    
    MPI_Barrier(MPI_COMM_WORLD);

    #pragma omp parallel private(tid, i,poff) shared(failed,epp)
    for(i = 0; i<niops; i++)
    {   
        if (i == 0)
        {
            tid = omp_get_thread_num();
            poff = MPI_rank * nthreads + tid;
        }
        #pragma omp critical
        if(mmap_addr_vec[offsets[tid][i].first][ ((offsets[tid][i].second.second)*epp) +poff ]==0)
            failed++;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    
    #pragma omp parallel private(tid, i,poff) shared(epp)
    for(i = 0; i<niops; i++)
    {
        if (i == 0)
        {
            tid = omp_get_thread_num();
            poff = MPI_rank * nthreads + tid;
        }
        mmap_addr_vec[offsets[tid][i].first][ ((offsets[tid][i].second.second)*epp) + poff ]--;
    }
    MPI_Barrier(MPI_COMM_WORLD);
    if (MPI_rank==0)
        cout<<"Done decremeting"<<endl;
    MPI_Barrier(MPI_COMM_WORLD);
  
    #pragma omp parallel private(tid, i,poff) shared(failed,epp)
    for(i = 0; i<niops; i++)
    { 
        if (i == 0)
        {
            tid = omp_get_thread_num();
            poff = MPI_rank * nthreads + tid;
        }
        #pragma omp critical
        if(mmap_addr_vec[offsets[tid][i].first][ ((offsets[tid][i].second.second)*epp) +poff ]!=0)
            failed++;
    }
    MPI_Barrier(MPI_COMM_WORLD);
  
    gettimeofday(&end, NULL);

    elapsed_time = double(end.tv_sec - start.tv_sec) + double(end.tv_usec - start.tv_usec)*1e-6;

    MPI_Allreduce(&failed,&global_failed, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&elapsed_time,&global_elapsed_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    
    if(MPI_rank == 0)
    {    
        if (global_failed!=0)
            cout<<"Validation FAILED and took" << global_elapsed_time<<" seconds for "<<niops*nprocesses*nthreads<<" I/O opertions on "<<nthreads<<" threads"<<endl;
        else 
            cout<<"Validation PASSED and took " << global_elapsed_time<<" seconds for "<<niops*nprocesses*nthreads<<" I/O opertions on "<<nthreads<<" threads"<<endl;
    }
    MPI_Barrier(MPI_COMM_WORLD);

    return 0;
}

void profile::cleanup_files()
{
    if(mmap_mode)
    {
        for (int i = 0; i< mmap_addr_vec.size(); i++)
        {
            munmap(mmap_addr_vec[i],mmap_size_vec[i]);
        }
    }

    //close;
    for (int i = 0; i< fd_vec.size(); i++)
        close(fd_vec[i]);

}

void profile::mmap_run()
{
    
    if(MPI_rank == 0)
        cout<<"---------MMAP MODE----------"<<endl;
    if (validate)
    {
        if(MPI_rank == 0)
            cout<<"VALIDATE MODE ---- PLEASE ENSURE INPUT FILES ARE ZERO'D"<<endl;
        mmap_validate_io();
        return;
    }
    //set max threads to nthreads to run i/o
    omp_set_num_threads(nthreads);

    //set up checksums, one for each thread
    vector<uint64_t> checksum(nthreads,0);
    uint64_t global_checksum;

    //time values
    timeval all_ioops, start, end;
    double elapsed_time, all_elapsed_time, global_elapsed_time, global_all_elapsed_time;

    int epp = 512 ; // entries per page   
    int tid=0;
    uint64_t i;

    MPI_Barrier(MPI_COMM_WORLD);
    gettimeofday(&all_ioops, NULL);
    #pragma omp parallel private(tid, i) 
    for(i = 0; i<niops; i++)
    {
        if (i == 0)
            tid = omp_get_thread_num();
        //primed ios
        if(i == skipped_ioops)
        {
            #pragma omp barrier
            if(tid==0)
                MPI_Barrier(MPI_COMM_WORLD);
            #pragma omp barrier
            if(tid ==0)
                gettimeofday(&start, NULL);
        }

        if( (mmap_size_vec[offsets[tid][i].first]/PAGE_SIZE)-1  < offsets[tid][i].second.second )
        {
            #pragma omp critical 
            //cout<<"PAGES:"<<mmap_size_vec[offsets[tid][i].first]/PAGE_SIZE<<endl;
            cout<<MPI_rank<<": OUT OF BOUNDS ERROR "<< &mmap_addr_vec[offsets[tid][i].first][offsets[tid][i].second.second]<< ", " << offsets[tid][i].second.second<<endl;
        }
        
        if (offsets[tid][i].second.first)//if write...
        {
            //cout<<"writing"<<endl;
            //cout<<offsets[tid][i].second.second<<endl;
    
            mmap_addr_vec[offsets[tid][i].first][offsets[tid][i].second.second*epp] = i;
        }
        else//else read
        {
            //cout<<"reading"<<endl;
            //cout<<offsets[tid][i].second.second<<endl;
            //cout<<"MPI_rank:"<<MPI_rank<<" index:"<< i<<" off: "<<offsets[tid][i].second.second<< "addr: "<<offsets[tid][i].first<<", at: "<<&mmap_addr_vec[offsets[tid][i].first][offsets[tid][i].second.second]<<endl;;
            checksum[tid]+= mmap_addr_vec[offsets[tid][i].first][offsets[tid][i].second.second*epp];
        }
        
    }
    // Close and munmap all files
    cleanup_files();
    gettimeofday(&end, NULL);
    MPI_Barrier(MPI_COMM_WORLD);

    //sum checksum from all threads
    for (int i = 1; i<checksum.size(); i++)
        checksum[0] += checksum[i];


    all_elapsed_time =   double(end.tv_sec - all_ioops.tv_sec) + double(end.tv_usec - all_ioops.tv_usec)*1e-6;
    elapsed_time =   double(end.tv_sec - start.tv_sec) + double(end.tv_usec - start.tv_usec)*1e-6;

    uint64_t totalniops = niops*nthreads;
    uint64_t primedniops = (niops-skipped_ioops)*nthreads;
    uint64_t global_totalniops, global_primedniops;
   
    MPI_Allreduce(&elapsed_time,&global_elapsed_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(&all_elapsed_time,&global_all_elapsed_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    MPI_Allreduce(&totalniops,&global_totalniops, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&primedniops,&global_primedniops, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&checksum[0],&global_checksum, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);

    if (MPI_rank == 0)
    {
        cout<<"RESULTS: " <<global_totalniops<<" I/O ops took " <<global_all_elapsed_time<<" seconds and "<<
            global_primedniops<< " I/O ops per thread took "<< global_elapsed_time<<" seconds" <<endl;
        cout<<"RESULTS: "<<nprocesses<<" procceses, and "<< nthreads<<" threads has IOPs:" << 
            global_totalniops/global_all_elapsed_time <<" primed IOPs: " << global_primedniops/global_elapsed_time<<endl;
    }
    //cout<<"RESULTS"<<MPI_rank<<": " << niops*nthreads<<" I/O ops took " <<all_elapsed_time<<" seconds and "<<
    //    (niops-skipped_ioops)*nthreads<< " I/O ops per thread took "<< elapsed_time<<" seconds\n" <<endl;
    //    MPI_rank<<": threads: "<< nthreads<<", checksum: "<<checksum[0]<<endl<<flush;

    MPI_Barrier(MPI_COMM_WORLD);

}
//same thing as mmap_except no check for out of bounds
//and uses pread
void profile::run()
{

    //set max threads to nthreads to run i/o
    omp_set_num_threads(nthreads);

    //set up checksums, one for each thread
    vector<uint64_t> checksum(nthreads,0);
    uint64_t global_checksum;

    //time values
    timeval all_ioops, start, end;
    double elapsed_time, all_elapsed_time, global_elapsed_time, global_all_elapsed_time;
   
    int tid=0;
    uint64_t i;
    vector<uint64_t*> buff(nthreads,NULL);

    for (int i = 0; i<nthreads; i++)
    {
        int err = posix_memalign( (void**)&buff[i], PAGE_SIZE,  access_size );
        if (err < 0)
            cout<<"ERROR: posix_memalign"<<err<<endl;
    }

    int count;
    MPI_Barrier(MPI_COMM_WORLD);
    gettimeofday(&all_ioops, NULL);
    #pragma omp parallel private(tid, i)
    for(i = 0; i<niops; i++)
    {
        if(i == 0)
            tid = omp_get_thread_num();
        
        if(i == skipped_ioops)
        {
            #pragma omp barrier
            if(tid==0)
                MPI_Barrier(MPI_COMM_WORLD);
            #pragma omp barrier
            if(tid ==0)
                gettimeofday(&start, NULL);
        }

        
        if (offsets[tid][i].second.first)//if write...
        {
      //      cout<<offsets[tid][i].second.second<<endl;
	    if(rmw_mode) {
	      count = pread(fd_vec[offsets[tid][i].first], buff[tid], access_size, offsets[tid][i].second.second*PAGE_SIZE);
	      buff[tid][tid%access_size] = i;
	    }
            count = pwrite(fd_vec[offsets[tid][i].first], buff[tid], access_size, offsets[tid][i].second.second*PAGE_SIZE);
            if (count != access_size)
            {
                cout<<"did not write size: " <<count <<" at " << offsets[tid][i].second.second
                <<" errno: " <<strerror(errno)<<endl;
            }
        }
        else//else read
        {
            count = pread(fd_vec[offsets[tid][i].first], buff[tid], access_size,offsets[tid][i].second.second*PAGE_SIZE);
            if (count != access_size)
            {
                cout<<"did not read size: " <<count <<" at " << offsets[tid][i].second.second
                <<" errno: " <<strerror(errno)<<endl;
            }
            checksum[tid]+= buff[tid][0];
        }
        
    }
    // Close all files
    cleanup_files();
    gettimeofday(&end, NULL);
    MPI_Barrier(MPI_COMM_WORLD);

    //sum checksum from all threads
    for (int i = 1; i<checksum.size(); i++)
        checksum[0] += checksum[i];


    all_elapsed_time =   double(end.tv_sec - all_ioops.tv_sec) + double(end.tv_usec - all_ioops.tv_usec)*1e-6;
    elapsed_time =   double(end.tv_sec - start.tv_sec) + double(end.tv_usec - start.tv_usec)*1e-6;

    uint64_t totalniops = niops*nthreads;
    uint64_t primedniops = (niops-skipped_ioops)*nthreads;
    uint64_t global_totalniops, global_primedniops;

    MPI_Allreduce(&elapsed_time,&global_elapsed_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);
    MPI_Allreduce(&all_elapsed_time,&global_all_elapsed_time, 1, MPI_DOUBLE, MPI_MAX, MPI_COMM_WORLD);

    MPI_Allreduce(&totalniops,&global_totalniops, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&primedniops,&global_primedniops, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);
    MPI_Allreduce(&checksum[0],&global_checksum, 1, MPI_LONG_LONG_INT, MPI_SUM, MPI_COMM_WORLD);

    if (MPI_rank == 0)
    {
        cout<<"checksum: "<<global_checksum<<endl;
        cout<<"RESULTS: "<<global_totalniops<<" I/O ops took " <<global_all_elapsed_time<<" seconds and "<<
            global_primedniops<< " I/O ops per thread took "<< global_elapsed_time<<" seconds" <<endl;
        cout<<"RESULTS: "<<nprocesses<<" procceses, and "<< nthreads<<" threads has IOPs:" << 
            global_totalniops/global_all_elapsed_time <<" primed IOPs: " << global_primedniops/global_elapsed_time<<endl;
    }
    //cout<<"RESULTS"<<MPI_rank<<": " << niops*nthreads<<" I/O ops took " <<all_elapsed_time<<" seconds and "<<
    //    (niops-skipped_ioops)*nthreads<< " I/O ops per thread took "<< elapsed_time<<" seconds\n" <<endl;
    //    MPI_rank<<": threads: "<< nthreads<<", checksum: "<<checksum[0]<<endl<<flush;

    MPI_Barrier(MPI_COMM_WORLD);

}


//array [R1, W1, R2, W2, .... ]
void profile::offset_summary(uint64_t* file_access)
{
    //for each thread...
    for (int i =0; i<offsets.size();i++)
    {
        //for each offset 
        for (int j =0; j<offsets[i].size();j++)
        {
            //increment reads or write of that file
            if(offsets[i][j].second.first)
                file_access[offsets[i][j].first*2+1]++;
            else
                file_access[offsets[i][j].first*2]++;
        }
    }
}

void profile::summary()
{
    uint64_t* off_sum = (uint64_t*) calloc(fd_vec.size()*2,sizeof(uint64_t));
 
    offset_summary(off_sum);
   
    //if first processs of each profile
    if(MPI_rank == p_offset)
    {

        MPI_Status Stat;
        //all threads should have the same number of files
        //collect stats from all processes in this profile
        uint64_t* tmp = (uint64_t*) calloc(2,sizeof(uint64_t));
        for (int i = (1+p_offset);i<(nprocesses+p_offset); i++)
        {
            MPI_Recv( (void*)tmp, fd_vec.size()*2,MPI_UNSIGNED_LONG_LONG,i, 0, MPI_COMM_WORLD, &Stat);
            //add to this processes totoal
            for(int j = 0; j<fd_vec.size();j++)
            {
                off_sum[j*2] += tmp[j*2];

                off_sum[(j*2)+1] += tmp[(j*2)+1];
                
            }
        }
        free(tmp);
        
        //wait until profile sends you a message
        if(MPI_rank >0 )
        {
            int x;
            MPI_Recv( (void*)&x, 1 ,MPI_INT, MPI_ANY_SOURCE, 5, MPI_COMM_WORLD, MPI_STATUS_IGNORE);
        }

        cout << "----------Profile Summary---------"<<endl;
        cout << "Name: " << name << endl;
        cout << "Processes: " << nprocesses << endl;
        cout << "Threads: " << nthreads << endl;
        cout << "Requested NIOP per thread: " <<niops<< endl;
        cout << "Skipped NIOP per thread: " <<skipped_ioops << endl;
        cout << "Timed NIOP per thread: " <<(niops - skipped_ioops)<< endl;
        cout << "mmap_mode: "<< mmap_mode<<endl;
        cout << "direct_mode: "<<direct_mode<<endl;
        cout << "rmw_mode: "<<rmw_mode<<endl;
        cout << "Number of files: " << name_list.size()<<endl;
        
        if(mmap_mode)
        {
            //more printing
            for (int i =0; i<name_list.size(); i++)
            {
                if(mmap_size_vec[i] > 536870912)//larger than 0.5GB
                {
                    cout<<"     "<<name_list[i]<<" at "<<mmap_addr_vec[i]<<"  for "<< 
                        (mmap_size_vec[i]/(1024*1024*1024.0)) <<  " GB"<<endl;
                }
                else
                {
                    cout<<"     "<<name_list[i]<<" at "<<mmap_addr_vec[i]<<"  for "<< 
                        (mmap_size_vec[i]/(1024*1024.0)) <<  " MB"<<endl;
                }
                cout<<"          There was "<<off_sum[i*2]<<" reads to this file and "<<
                    off_sum[(i*2)+1]<<" writes to this file."<<endl << flush;
            }
        }
        else
        {
            //more printing
            for (int i =0; i<name_list.size(); i++)
            {
                if(mmap_size_vec[i] > 536870912)//larger than 0.5GB
                {
                    cout<<"     "<<name_list[i]<<"  for "<< 
                        (mmap_size_vec[i]/(1024*1024*1024.0)) <<  " GB"<<endl;
                }
                else
                {
                    cout<<"     "<<name_list[i]<<"  for "<< 
                        (mmap_size_vec[i]/(1024*1024.0)) <<  " MB"<<endl;
                }
                cout<<"          There was "<<off_sum[i*2]<<" reads to this file and "<<
                    off_sum[(i*2)+1]<<" writes to this file."<<endl << flush;
            }
        }
        cout<<"Number of generators: "<<generators.size()<<endl;
        generators[0]->summary();

        

        //send to the next profile to start
        if (MPI_size > (MPI_rank+nprocesses))
        {
            flush(cout);
            MPI_Send( (void*) &MPI_rank, 1, MPI_INT, (MPI_rank+nprocesses), 5,MPI_COMM_WORLD);
        }
    }
    else //send to the process
        MPI_Send( (void*)off_sum, fd_vec.size()*2, MPI_UNSIGNED_LONG_LONG, p_offset, 0,MPI_COMM_WORLD);

    free(off_sum);
}



//populate the offsets fields
//fills nthreads vectors one at time in parallel using all generators
//will not work for seqential generation
//deprecated
void profile::fill_offsets1()
{
    timeval start, end;
    double elapsed_time;
    
    //number threads = gnerators
    omp_set_num_threads(generators.size());
    int tid=0;

    
    gettimeofday(&start, NULL);
    //for each thread that will run the i/o
    for (int i = 0; i<nthreads; i++) 
    {   
        //make the empty vector
        vector<file_access> tmp;
        offsets.push_back(tmp);

        //make it the right size            
        offsets[i].resize(niops);

        //each generator fills a block
        #pragma omp parallel for private(tid) schedule(static)
        for(int j = 0; j<niops; j++)
        {   
            tid = omp_get_thread_num();

            offsets[i][j] = generators[tid]->gen_faccess() ;
        }
    }   
    gettimeofday(&end, NULL);

    elapsed_time =   double(end.tv_sec - start.tv_sec) + double(end.tv_usec - start.tv_usec)*1e-6;
    
    cout<<"Generation on "<<MPI_rank<<" took "<<elapsed_time<<" seconds"<<endl;
}

//deprecated functionally.... 
//needs to be called by run(), when wrtes wrote check sum
int profile::validate_io2()
{
    //set max threads to nthreads to run i/o
    omp_set_num_threads(nthreads);

    //set up checkpoints, one for each thread
    vector<uint64_t> checksum(nthreads,0);

    boost::unordered_set<uint64_t> failed_offset = boost::unordered_set<uint64_t> ();

    boost::unordered_set<uint64_t> passed_offset = boost::unordered_set<uint64_t> ();


    cout<<"max size"<<failed_offset.max_size()<<endl;
    //time values
    timeval start, end;
    double elapsed_time;
   
    
//    cout<<"base ptr is "<<mmap_addr_vec[0]<<endl;

    int tid=0;
    uint64_t* ptr = mmap_addr_vec[0]; 
    
    MPI_Barrier(MPI_COMM_WORLD);
    gettimeofday(&start, NULL);
    //parallel i/o, reduction is not used to minimize interference
    int ret = 0; 

    cout<<"starting replay"<<endl;
    int i;
#pragma omp parallel shared (failed_offset, passed_offset) private (i,tid)
//for (int tid = 0; tid<nthreads;tid++)
{
    for( i = 0; i<niops; i++)
    {
        tid = omp_get_thread_num();
        if (offsets[tid][i].second.first)
        {
            /*
            if(offsets[tid][i].second.second == 345080988)
            {
                #pragma omp crtical 
                cout<<tid<< "does a "<< offsets[tid][i].second.first <<" at " << i<<" and sees a " << mmap_addr_vec[offsets[tid][i].first][offsets[tid][i].second.second]<<endl;
            }*/
            if (mmap_addr_vec[offsets[tid][i].first][offsets[tid][i].second.second] != i)
            {
                #pragma omp critical
                {
                    failed_offset.insert(offsets[tid][i].second.second);
                }
            }
            else
            {
                #pragma omp critical
                {
                    failed_offset.erase(offsets[tid][i].second.second);
                    passed_offset.insert(offsets[tid][i].second.second);
                }
            }
        }/*
        else
            checksum[tid]+= mmap_addr_vec[offsets[tid][i].first][offsets[tid][i].second.second];
    */
    }
}
    MPI_Barrier(MPI_COMM_WORLD);

    gettimeofday(&end, NULL);
   
    elapsed_time =   double(end.tv_sec - start.tv_sec) + double(end.tv_usec - start.tv_usec)*1e-6;
    cout<<passed_offset.size()<<"going to erase in passed "<<failed_offset.size()<<endl;


    int failed = 0;
    for (boost::unordered_set<uint64_t>::iterator iter = failed_offset.begin() ; iter != failed_offset.end(); iter++)
    {
        uint64_t x = *iter;
        int i = passed_offset.erase(x);
        if (i <= 0 )
        {
 //           cout<<"failed for "<<x<<", "<<i<<endl;
            failed = 1;
        }
        else
            failed_offset.erase(x);
    }

    if (failed == 0)
        cout<<"On rank "<<MPI_rank<<" validate PASSED and took " << elapsed_time<<" seconds."<<endl;
    else
        cout<<"On rank "<<MPI_rank<<" validate FAILED and took " << elapsed_time<<" seconds with size of "<<failed_offset.size()<<endl;
    //cout<<"size is "<<failed_offset.size()<<endl;
    
   /* 
    if (failed_offset.size())
        cout<<"On rank "<<MPI_rank<<" validate FAILED and took " << elapsed_time<<" seconds."<<endl;
    else
        cout<<"On rank "<<MPI_rank<<" validate PASSED and took " << elapsed_time<<" seconds."<<endl;
    */
    MPI_Barrier(MPI_COMM_WORLD);

    return 0; 

}


/*
int main()
{
    timeval start, end;
    double elapsed_time;
    
    profile test2 = profile(8, 0, "/mnt/virident/henry/32GB", "uni", "0", "0", "4294967296", "1", 50000, 16);
    
    test2.fill_offsets();A
   // test2.run();
}*/
