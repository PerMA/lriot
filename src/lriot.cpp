/*
 * Copyright (c) 2013, Lawrence Livermore National Security, LLC. 
 * Produced at the Lawrence Livermore National Laboratory. 
 * Written by Henry Hsieh <hsieh7@llnl.gov> and Brian Van Essen <vanessen1@llnl.gov>. 
 * LLNL-CODE-624712. 
 * All rights reserved.
 * 
 * This file is part of LRIOT, Version 1.0. 
 * For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html
 * 
 * Please also read this link – Additional BSD Notice. 
 * 
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 * 
 * • Redistributions of source code must retain the above copyright notice, this
 *   list of conditions and the disclaimer below.
 * 
 * • Redistributions in binary form must reproduce the above copyright notice,
 *   this list of conditions and the disclaimer (as noted below) in the
 *   documentation and/or other materials provided with the distribution.
 * 
 * • Neither the name of the LLNS/LLNL nor the names of its contributors may be
 *   used to endorse or promote products derived from this software without
 *   specific prior written permission.
 * 
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL LAWRENCE LIVERMORE NATIONAL SECURITY, LLC,
 * THE U.S. DEPARTMENT OF ENERGY OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 * LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
 * ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 * 
 * 
 * Additional BSD Notice
 * 
 * 1. This notice is required to be provided under our contract with the
 * U.S. Department of Energy (DOE). This work was produced at Lawrence Livermore
 * National Laboratory under Contract No. DE-AC52-07NA27344 with the DOE.
 * 
 * 2. Neither the United States Government nor Lawrence Livermore National
 * Security, LLC nor any of their employees, makes any warranty, express or
 * implied, or assumes any liability or responsibility for the accuracy,
 * completeness, or usefulness of any information, apparatus, product, or
 * process disclosed, or represents that its use would not infringe
 * privately-owned rights.
 * 
 * 3. Also, reference herein to any specific commercial products, process, or
 * services by trade name, trademark, manufacturer or otherwise does not
 * necessarily constitute or imply its endorsement, recommendation, or favoring
 * by the United States Government or Lawrence Livermore National Security,
 * LLC. The views and opinions of authors expressed herein do not necessarily
 * state or reflect those of the United States Government or Lawrence Livermore
 * National Security, LLC, and shall not be used for advertising or product
 * endorsement purposes.
 * 
 */
#include "profile.hpp"

#include <boost/program_options.hpp>
#include <boost/lexical_cast.hpp>
#include <boost/algorithm/string.hpp>
namespace po = boost::program_options;

#include "mpi.h"

#include <iostream>
#include <string.h>
#include <fstream>
#include <iterator>
using namespace std;

#define PAGE_SIZE 4096
#define DEFAULT_PROCESSES 1
#define DEFAULT_THREADS 4
#define DEFAULT_MODE "mmap"
#define DEFAULT_SIZE PAGE_SIZE
#define DEFAULT_READ "*1"
#define DEFAULT_DELAY 0.06f
#define DEFAULT_NIOPS 1000
//not used 
//#define DEFAULT_FILENAME "/dev/file1:/dev/file2"
#define DEFAULT_PATTERN "*"
#define DEFAULT_OFFSET "*"
#define DEFAULT_RANGE "*"
#define DEFAULT_WEIGHT "*"
#define DEFAULT_SEED 123

//global rank/size
int MPI_rank, MPI_size;

//used for mutiple profiles to denote the first of each profile
int process_offset = 0;

//grab the params and retun by refernce as a vector of all
//each process will have a copy of its own profile
//variable i is the index into the config file variableble map (matched by name)
int parse_profile(po::variables_map& cmd, po::variables_map& cf, int i, vector<profile*>& prof_ptr_vec)
{
    if (MPI_rank ==0 )
        cout<<"-------------------------------------------------------"<<endl;
    
    string stmp; 
    float ftmp;       
    
    string name = "";
    string mode, filename, pattern, weight, offset, range, read;
    int nprocesses,nthreads, size, seed, NIOP;
    bool vali,unique;
    float delay; 
  

    //get from command line first, otherwise get from config files
    //otherwise get from default 
   
    //name
    if (cf.count("name"))
        name = cf["name"].as<vector <string> >()[i];

    //process
    if (cmd.count("processes"))
        nprocesses = cmd["processes"].as<vector <int> >()[0];
    else if (cf.count("processes"))
        nprocesses = cf["processes"].as<vector <int> >()[i];
    else
      	nprocesses = MPI_size; //DEFAULT_PROCESSES;
    
    //threads
    if (cmd.count("threads"))
        nthreads = cmd["threads"].as<vector <int> >()[0];
    else if (cf.count("threads")) 
        nthreads = cf["threads"].as<vector <int> >()[i];
    else 
        nthreads = DEFAULT_THREADS;
    
    //NIOP
    if (cmd.count("NIOP"))
        NIOP = cmd["NIOP"].as<vector <int> >()[0];
    else if (cf.count("NIOP"))
        NIOP= cf["NIOP"].as<vector <int> >()[i];
    else
        NIOP = DEFAULT_NIOPS;

    //mode
    if (cmd.count("mode"))
    {
        stmp = cmd["mode"].as<vector <string> >()[0];
        if (stmp == "mmap" || stmp == "direct" || stmp == "direct_rmw" || stmp == "std")
            mode = stmp;
        else
        {
            if(MPI_rank == 0)
                cout<<"ERROR: "<<stmp << " is not a valid mode"<<endl;
            return -1;
        }
    }
    else if(cf.count("mode"))
    {
        stmp = cf["mode"].as<vector <string> >()[i];
        if (stmp == "mmap" || stmp == "direct" || stmp == "direct_rmw" || stmp == "std")
            mode = stmp;
        else
        {
            if(MPI_rank ==0)
                cout<<"ERROR: "<<stmp << " is not a valid mode"<<endl;
            return -1;
        }
    }
    
    //size 
    if (cmd.count("size"))
        size = cmd["size"].as<vector <int> >()[0];
    else if (cf.count("size"))
        size = cf["size"].as<vector <int> >()[i];
    else
        size = DEFAULT_SIZE;

    //read
    if (cmd.count("read"))
    {   
        read = cmd["read"].as<vector <string> >()[0];
    }
    else if (cf.count("read"))
    {
        read = cf["read"].as<vector <string> >()[i];
    }
    else
        read = DEFAULT_READ;

    //delay
    if (cmd.count("delay"))
    {   
        ftmp = cmd["delay"].as<vector <float> >()[0];
        if (ftmp >= 0.0f && ftmp <= 1.0f)
            delay = ftmp; 
        else
        {
            cout<<"ERROR: "<<ftmp<<" is an invalid delay percent"<<endl;
            return -2;
        }
    }
    else if (cf.count("delay"))
    {
        ftmp = cf["delay"].as<vector <float> >()[i];
        if (ftmp >= 0.0f && ftmp <= 1.0f)
            delay = ftmp; 
        else
        {
            cout<<"ERROR: "<<ftmp<<" is an invalid delay percent"<<endl;
            return -2;
        }
    }
    else
    {
        delay = DEFAULT_DELAY;
    }

    //filename
    if (cmd.count("filename"))
        filename = cmd["filename"].as<vector <string> >()[0];
    else if (cf.count("filename"))
        filename = cf["filename"].as<vector <string> >()[i];
    else
    {
        cout<<"ERROR: filenames not specified"<<endl;
        return -3;
    }

    //pattern
    if (cmd.count("pattern"))
    {
        pattern = cmd["pattern"].as<vector <string> >()[0];
    }
    else if (cf.count("pattern"))
    {
        pattern = cf["pattern"].as<vector <string> >()[i];
    }
    else
    {
        pattern = DEFAULT_PATTERN;
    }

    //offset
    if (cmd.count("offset"))
    {
        offset = cmd["offset"].as<vector <string> >()[0];
    }
    else if (cf.count("offset"))
    {
        offset = cf["offset"].as<vector <string> >()[i];
    }
    else
    {
        offset = DEFAULT_OFFSET;
    }

    //range
    if (cmd.count("range"))
    {
        range = cmd["range"].as<vector <string> >()[0];
    }
    else if (cf.count("range"))
    {
        range = cf["range"].as<vector <string> >()[i];
    }
    else
    {
        range = DEFAULT_RANGE;
    }
    
    //weight
    if (cmd.count("weight"))
    {
        weight = cmd["weight"].as<vector <string> >()[i];
    }
    else if (cf.count("weight"))
    {
        weight  = cf["weight"].as<vector <string> >()[i];
    }
    else
    {
        weight = DEFAULT_WEIGHT;
    }

    //seed
    if (cmd.count("seed"))
        seed = cmd["seed"].as<vector <int> >()[0];
    else if (cf.count("seed"))
        seed  = cf["seed"].as<vector <int> >()[i];
    else
        seed = DEFAULT_SEED;

    //validate flag
    if (cmd.count("validate"))
        vali= true;
    else
        vali=false;

    //unique flag
    if (cmd.count("unique"))
        unique= true;
    else
        unique=false;

    uint64_t t_NIOP = (NIOP)/(nprocesses*nthreads);
    if (t_NIOP*nprocesses*nthreads != NIOP)
    {
        NIOP = t_NIOP*nprocesses*nthreads;
        if (MPI_rank==0)
            cout<<"WARNING: I/O's is not evenly divisable by the total number of threads, set to "<< NIOP<<endl;
    }

    if(mode=="mmap"|| vali)
    {
        if(size != PAGE_SIZE)
        {
	  if(MPI_rank==0)
	    cout<<"WARNING: illegal size ("<<size<<") specified for "<<mode<<" mode, setting access size to page size, " <<PAGE_SIZE<<endl;
	  
	  size = PAGE_SIZE;
        }
    }else if(mode=="direct" || mode == "direct_rmw")
    {
        if(size % PAGE_SIZE != 0)
        {
	  int new_size = ((size / PAGE_SIZE) + 1) * PAGE_SIZE;
	  if(MPI_rank==0)
	      cout<<"WARNING: illegal size ("<<size<<") specified for "<<mode<<" mode, setting access size to a multiple of page size, " <<new_size<<endl;
	  size = new_size;
        }
    }

    //print out
    if(name == "")
        name = "COMMAND LINE ARG ONLY";
    
    if(MPI_rank == 0)
    {
        cout<<"Name: " <<name<<endl;
        cout<<"Processes: " <<nprocesses<<endl;
        cout<<"Threads per process: " <<nthreads<<endl;
        cout<<"Total I/O operations: " <<NIOP<<endl;
        cout<<"I/O operations per thread: " <<t_NIOP<<endl;
        cout<<"Size of accesses: " <<size<<endl;
        cout<<"Percent of reads: " <<read<<endl;
        cout<<"Delay percent: " <<delay<<endl;
        cout<<"Mode: "<<mode<<endl;
        cout<<"Filenames: " <<filename<<endl;
        cout<<"Pattern: " <<pattern<<endl;
        cout<<"Offset: " <<offset<<endl;
        cout<<"Weight: " <<weight<<endl;
        cout<<"Range: " <<range<<endl;
        cout<<"Seed: " <<seed<<endl;
        cout<<"Validate: "<<vali<<endl;
        cout<<"Unique: "<<unique<<endl;
    }   
    sleep(.5) ;
    MPI_Barrier(MPI_COMM_WORLD);
    //make a new profile for each process. 
    if(MPI_rank < process_offset+nprocesses && MPI_rank >= process_offset )
//    for(int i = 0; i<nprocesses; i++)
    {
        prof_ptr_vec.push_back( new profile(nprocesses,process_offset, mode,
            nthreads, seed+MPI_rank, filename,pattern, read, offset, range, weight,
            t_NIOP, size, name, vali,unique, delay,size) );
    }
   
    process_offset +=nprocesses;
    
    return 0;
}

void MPI_run_profile(int rank, vector<profile* > prof_ptr_vec)
{
    if(prof_ptr_vec[0]->is_bad())
        return;
    MPI_Barrier(MPI_COMM_WORLD);
    //initialize
    if(MPI_rank == 0)
        cout<<"--------INITIALZING PROFILES-----------"<<endl<<flush;
    sleep(1);
    MPI_Barrier(MPI_COMM_WORLD);
    
    prof_ptr_vec[0]->init();
    
    if(prof_ptr_vec[0]->is_bad())
        return;
    MPI_Barrier(MPI_COMM_WORLD);
   
    //start running benchmakrs
    sleep(1);
    if(MPI_rank == 0)
        cout<<"--------STARTING I/O benchmark-----------"<<endl<<flush;
    MPI_Barrier(MPI_COMM_WORLD);
    
    if (prof_ptr_vec[0]->is_mmap())
        prof_ptr_vec[0]->mmap_run();
    else
        prof_ptr_vec[0]->run();

    MPI_Barrier(MPI_COMM_WORLD);
    
    sleep(1);
    if(MPI_rank == 0)
        cout<<"--------END OF BENCHMARK-----------"<<endl<<flush;
    MPI_Barrier(MPI_COMM_WORLD);
    
    //summerize
    prof_ptr_vec[0]->summary();
    
    MPI_Barrier(MPI_COMM_WORLD);

    if(MPI_rank == 0)
        cout<<"----------END OF SUMMARY-------------"<<endl<<flush;
}

int main(int argc, char* argv[])
{
    //starting mpi
    int rc = MPI_Init(&argc, &argv);
    if (rc != MPI_SUCCESS)
    {
        if(MPI_rank == 0)
            cout<<"Error starting MPI Program. Terminating."<<endl;
        MPI_Abort(MPI_COMM_WORLD,rc);
        return 1;
    }

    //get size and rank, globals
    MPI_Comm_size(MPI_COMM_WORLD, &MPI_size);
    MPI_Comm_rank(MPI_COMM_WORLD, &MPI_rank);

    string config_files="";
    string profiles;

    //comand line only options
    po::options_description generic("Commandline only options");
    generic.add_options()
        ("version", "Print version")
        ("help,h", "Print help message")
        ("config,c", 
            po::value<string>(&config_files),
            "Name of the config file")
        ("validate,v", "Validate I/O")
        ("unique,u", "Enforce unique sequences for a single processes");
    ;

    //config only option
    po::options_description config_only("Config only options");
    config_only.add_options()
        ("name,n",po::value<vector <string> >() -> composing(),"Name of profile")
    ;

    //benchmark options
    //composing = appendable = multiple entries in config file
    //defaults inserted in parse_profile
    po::options_description options("Benchmark options");
    options.add_options()
        ("processes,n", po::value< vector<int> >() -> composing(),
            // -> default_value(vector<int>(1,DEFAULT_PROCESSES),boost::lexical_cast<string> (DEFAULT_PROCESSES) ),
            "Number of processes")

        ("threads,t", po::value< vector<int> >() -> composing(),
            //-> default_value(vector<int>(1,DEFAULT_THREADS),boost::lexical_cast<string> (DEFAULT_THREADS) ),
            "Number of threads per process")

        ("NIOP,i", po::value< vector<int> >() -> composing(),
            //-> default_value(vector<int>(1,DEFAULT_NIOPS),boost::lexical_cast<string> (DEFAULT_NIOPS) ),
            "Total number of I/O operations")

        ("mode,m", po::value< vector<string> >() -> composing(),
            //-> default_value(vector<string>(1,DEFAULT_MODE),boost::lexical_cast<string> (DEFAULT_MODE) ),
            "I/O mode select from [mmap,direct,direct_rmw,std]")

        ("size,s", po::value< vector<int> >() -> composing(),
            //-> default_value(vector<int>(1,DEFAULT_SIZE),boost::lexical_cast<string> (DEFAULT_SIZE) ),
            "Size of memory access in bytes only used in std io")

        ("read,k", po::value< vector<string> >() -> composing(),
            //-> default_value(vector<float>(1,DEFAULT_READ),boost::lexical_cast<string> (DEFAULT_READ) ),
            "Percent of accesses that are reads for each file [0-1], delineated by :")

        ("delay,d", po::value< vector<float> >() -> composing(),
            //-> default_value(vector<float>(1,DEFAULT_DELAY),boost::lexical_cast<string> (DEFAULT_DELAY) ),
            "Percent of I/O operations before timing starts [0-1]")

        ("filename,f", po::value< vector<string> >() -> composing(),
            //-> default_value(vector<string>(1,DEFAULT_FILENAME),boost::lexical_cast<string> (DEFAULT_FILENAME) ),
            "Files to be accessed, delineated by :")

        ("pattern,p", po::value< vector<string> >() -> composing(),
            //-> default_value(vector<string>(1,DEFAULT_PATTERN),boost::lexical_cast<string> (DEFAULT_PATTERN) ),
            "Pattern to generate [uni,norm, or 'seq|stride|end|jump|thread_offset'],delineated by : e.g. 'seq|1|*|1|1'")

        ("offset,o", po::value< vector<string> >() -> composing(),
            //-> default_value(vector<string>(1,DEFAULT_OFFSET),boost::lexical_cast<string> (DEFAULT_OFFSET) ),
            "Offset into each file, delineated by :")

        ("range,r", po::value< vector<string> >() -> composing(),
            //-> default_value(vector<string>(1,DEFAULT_RANGE),boost::lexical_cast<string> (DEFAULT_RANGE) ),
            "Ranges for each pattern, delineated by :")

        ("weight,w", po::value< vector<string> >() -> composing(),
            //-> default_value(vector<string>(1,DEFAULT_WEIGHT),boost::lexical_cast<string> (DEFAULT_WEIGHT) ),
            "Weights of of each pattern, delineated by :")

        ("seed,e", po::value< vector<int> >() -> composing(),
            //-> default_value(vector<int>(1,DEFAULT_SEED),boost::lexical_cast<string> (DEFAULT_SEED) ),
            "Seed for random generation");
    ;

    //options not for view
    //name of the profiles to run
    po::options_description hidden("Hidden options");
    hidden.add_options()
        ("profile", po::value<string>(&profiles), "select profile by name")
    ;

    //command line options
    po::options_description cmdline_options;
    cmdline_options.add(generic).add(options).add(hidden);

    //viewable options aka help
    po::options_description visible("Alowed options");
    visible.add(generic).add(options);

    //config file options
    po::options_description config_file_options;
    config_file_options.add(options).add(hidden).add(config_only);

    //input args, profile name
    po::positional_options_description p;
    p.add("profile",-1);


    //parse commmand line args
    po::variables_map cmd;
    store(po::command_line_parser(argc, argv).options(cmdline_options).
        positional(p).run(),cmd);
    notify(cmd);

    //print help mesage
    if(cmd.count("help"))
    {
        if (MPI_rank == 0)
        {
            cout<<"Usage: \n./LRIOT <options> :comand line mode\nOR \n " <<
                "./LRIOT <options> -c <config file> <profile name>: command line override mode\nOR \n" <<
                "./LRIOT -c <config file> <bevhaior name(s) delinated by :> :mutiple profile mode\n\n";
            cout << visible <<endl;
        }
        MPI_Finalize();
        return 0;
    }

    //version message
    if(cmd.count("version"))
    {
        if (MPI_rank == 0)
            cout<<"Livermore Random I/O Testbench, version 2.0"<<endl;
        MPI_Finalize();
        return 0;
    }
    
    int error = 0;   
    vector<profile* >  prof_ptr_vec;

    //test cmd line args
    if(cmd.count("config"))
    {
        //no config file = commandline overide or mutiple profile
        if(MPI_rank == 0)
        {   
            cout<<"Config file is : "<<config_files<<endl;
            cout<<"Using profile(s): "<<profiles<<endl;
        }

        po::variables_map cf;

        string config_file;
        vector<string> config_file_list;
        boost::split( config_file_list, config_files, boost::is_any_of(":"), boost::token_compress_on );
       
        //for open each config file and parse
        for (int i = 0; i<config_file_list.size();i++)
        {
            config_file = config_file_list[i];
            ifstream ifs(config_file.c_str());
            if(!ifs)
            {
                if(MPI_rank==0)
                    cout<<"ERROR: can not open config file " <<config_file<<" with error: "<<strerror(errno)<<endl;
                MPI_Finalize();
                return -1;
            }
        
            store(parse_config_file(ifs,config_file_options), cf);
            notify(cf);
            ifs.close();
        }

        string profile_name;
        vector<string> profile_list;
        boost::split( profile_list, profiles, boost::is_any_of(":"), boost::token_compress_on );

        //for each of the profiles listed
        for (int j = 0; j < profile_list.size() && error == 0; j++)
        {
            profile_name = profile_list[j];
            int flag = 1;
        
            //linear search for it in the config file
            for (int i = 0; i<cf["name"].as<vector <string> >().size(); i++)
            {
                if (profile_name ==  cf["name"].as<vector <string> >()[i])
                {
                    flag = 0;
                    error += parse_profile(cmd,cf,i, prof_ptr_vec);
                    break;
                }
            }
            if (flag)
            {
                if(MPI_rank == 0)
                    cout<<"ERROR: Profile "<<profile_name<<" not found"<<endl;
                error--;
                break;
            }
        }


    }
    else
    {
        //no config file = commandline only
        if(MPI_rank == 0)
            cout<<"No config file given.... command line only mode"<<endl;
        error += parse_profile(cmd,cmd,0, prof_ptr_vec);
    }

    //no error in creating run
    if(error==0)
        MPI_run_profile(MPI_rank, prof_ptr_vec); 
    
    //clean up
    for(int i = 0; i< prof_ptr_vec.size(); i++)
    {
        delete prof_ptr_vec[i];
    }

    MPI_Finalize();	
    return error;
}
