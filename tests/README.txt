Copyright (c) 2013, Lawrence Livermore National Security, LLC. 
Produced at the Lawrence Livermore National Laboratory. 
Written by Roger Pearce <pearce7@llnl.gov>. 
LLNL-CODE-624712. 
All rights reserved.

This file is part of LRIOT, Version 1.0. 
For details, see https://computation.llnl.gov/casc/dcca-pub/dcca/Downloads.html

--------------------------------------------------------------------------------

This is a stand alone MMAP and NVRAM benchmark that is designed to test the 
performance of the mmap system stack for simple out-of-core data manipulation tasks.   
The benchmark forks a set of processes, creates independent files for each process, 
and performs four basic tasks.  Each of the four tasks are independently timed 
and reported.  

1)  Data Initialization - A sequential sequence of integers are written out to each 
                          process' private file.
2)  Scramble  - Each process shuffles their private data using c++ std::random_shuffle()
3)  Sort      - Each process sorts their private data using c++ std::sort()
4)  Validate  - Each process verifies their private data, ensuring the original 
                sequential sequence appears.
                
To build:  
  c++ mmap_nvram_bench.cpp -O3 -o mmap_nvram_bench

To run:  
  ./mmap_nvram_bench <base_filename> <total GBytes> <num workers>

where: 
<base_filename> contains the path to the device to mmap.  The actual filenames created 
                will include the process rank for process specific files.
<toal GBytes>   (float) is the total amount of data to generate among all the processes combined.
                It is intended that this size exceeds the size of main-memory (DRAM) to test
                the out-of-core perfomance.
<num workers>   The number of worker processes to fork.


Example output for 10GB of data and 24 processes:

bash-4.1$ ./mmap_nvram_bench /l/ssd/bench 10 24
Files created: 0.00335217 seconds.
Data Initialized: 0.530847 seconds.
Files scrambled: 6.45084 seconds.
Files Sorted: 6.89638 seconds.
Files Validated: 0.25079 seconds.
